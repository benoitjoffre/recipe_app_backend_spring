package app.cook.cookapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DishIdException extends RuntimeException {

    public DishIdException(String message) {
        super(message);
    }
}
