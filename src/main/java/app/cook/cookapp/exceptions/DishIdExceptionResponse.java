package app.cook.cookapp.exceptions;

public class DishIdExceptionResponse {

    private String dishId;

    public DishIdExceptionResponse(String dishId) {
        this.dishId = dishId;
    }


    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }
}
