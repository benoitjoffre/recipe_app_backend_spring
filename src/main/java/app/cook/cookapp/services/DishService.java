package app.cook.cookapp.services;

import app.cook.cookapp.Dish;
import app.cook.cookapp.exceptions.DishIdException;
import app.cook.cookapp.repositories.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DishService {

    @Autowired
    private DishRepository dishRepository;

    public Dish saveOrUpdateDish(Dish dish) {
        try{
            dish.setId(dish.getId());
            return dishRepository.save(dish);
        } catch(Exception e) {
            throw new DishIdException("Dish ID'"+dish.getId()+"' already exists");
        }
    }



    public Dish findDishById(Long dishId) {

        Dish dish = dishRepository.findDishById(dishId);

        if(dish == null) {
            throw new DishIdException("Dish ID'"+ dishId +"' does not exist");
        }
        return dish;
    }

    public Iterable<Dish> findAllDishes() {
        return dishRepository.findAll();
    }

    public void deleteDishById(Long dishId) {
        Dish dish = dishRepository.findDishById(dishId);

        if(dish == null) {
            throw new DishIdException("Cannot Dish with ID '" + dishId + "'This dish does not exist.");
        }

        dishRepository.delete(dish);
    }
}
