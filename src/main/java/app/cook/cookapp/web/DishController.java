package app.cook.cookapp.web;

import app.cook.cookapp.Dish;
import app.cook.cookapp.services.DishService;
import app.cook.cookapp.services.MapValidationErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private MapValidationErrorService mapValidationErrorService;

    @CrossOrigin(origins = "http://192.168.1.12:3000")
    @PostMapping("")
    public ResponseEntity<?> createNewDish(@Valid @RequestBody Dish dish, BindingResult result) {

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);
        if(errorMap!=null) return errorMap;

        Dish dish1 = dishService.saveOrUpdateDish(dish);
        return new ResponseEntity<Dish>(dish1, HttpStatus.CREATED);
    }

    @CrossOrigin(origins = "http://192.168.1.12:3000")
    @GetMapping("/{dishId}")
    public ResponseEntity<?> getDishById(@PathVariable Long dishId) {
        Dish dish = dishService.findDishById(dishId);
        return new ResponseEntity<Dish>(dish, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://192.168.1.12:3000")
    @GetMapping("/all")
    public Iterable<Dish> getAllDishes(){
        return dishService.findAllDishes();
    }

    @CrossOrigin(origins = "http://192.168.1.12:3000")
    @DeleteMapping("/{dishId}")
    public ResponseEntity<?> deleteProject(@PathVariable Long dishId) {
        dishService.deleteDishById(dishId);

        return new ResponseEntity<String>("Dish with ID: "+dishId+" was deleted !", HttpStatus.OK);
    }

}
