package app.cook.cookapp.repositories;


import app.cook.cookapp.Dish;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DishRepository extends CrudRepository<Dish, Long> {

    Dish findDishById(Long dishId);

    @Override
    Iterable<Dish> findAll();
}
