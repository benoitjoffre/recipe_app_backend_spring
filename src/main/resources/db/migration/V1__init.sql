CREATE TABLE dishes (
  id int(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  origin varchar(255) NOT NULL,
  ingredients ARRAY[1000] DEFAULT NULL,
  urlImg varchar(255) DEFAULT NULL,
  created_at timestamp NULLABLE,
  updated_at timestamp NULLABLE,
  PRIMARY KEY (id),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;